# CoreOs Configuration


## Enable the Remote API on a New Socket
https://coreos.com/docs/launching-containers/building/customizing-docker/


docker pull zeograd/jenkins-slave


docker run -it -d --name shipyard-rethinkdb-data --entrypoint /bin/bash shipyard/rethinkdb -l


docker run -it -P -d --name shipyard-rethinkdb --volumes-from shipyard-rethinkdb-data shipyard/rethinkdb

docker run -it -p 8080:8080 -d --name shipyard --link shipyard-rethinkdb:rethinkdb shipyard/shipyard