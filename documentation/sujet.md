 # Définition du Sujet 

 Code source privé pour le moment sur BitBucket
 
 Markown sur bitbucket



# Application Skeleton : Hello World from DB

## Un test d'acceptance : 

  Qui dit qu'il faut avoir un *hello "World"* avec "World" qui viens de la base de données


## Toute la stack de Build

### Plateforme :

 * Vagrant
 * CoreOS
 * Docker

### Images Docker à utiliser (Pull)

 * Image Jenkins
     - 8080 (http) 
     - JENKINS_PORT / JENKINS_URI (IP) / JENKINS_HOME
 * Image GitLab
     - 8000 (http) 
 * Image Nexus
     - 9000 (http)
 * Image Registry
     - 5000 (http)

### Images Docker à construire 

 * Image Build Java (Compilation / UT / IT )
 	* Java (Oracle 8)
 	* Maven
 	* ???
 * Image Build JavaScript (UT / IT)
 	* Node
 	* Grunt 	
 	* Bower
 * Image Deploy Application (Frontend et Backend)
 	* Java (Oracle 8)
 	* Apache Tomcat
 	* Apache Httpd
 	* Configuration Apache Httpd
 * Image Deploy BDD
 	* Mongo DB (Image Officelle)
 * Image Data Container 
 	* BusyBox
 * Image Acceptance Tests ( JsRunner, Cucumber Js)
 	* From Build JavaScript
 	* PhantomJs
 	* Cucumber JS
 	* Wedriver.Io

 * Image Deploy BDD (Production) 
 	* (autre instance de celle du @run)
 * Image Data Container (Production)
 	* (autre instance de celle du @run)
 * Image Deploy Application (Production) (Frontend et Backend)
 	* (autre instance de celle du @run)
 