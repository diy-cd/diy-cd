#!/bin/bash

BASEDIR=/home/core

REGISTRY_PORT=5000
REGISTRY_URI=http://127.0.0.1
REGISTRY_HOME=$BASEDIR/registry


docker pull registry
mkdir -p $REGISTRY_HOME
docker run -d -p 5000:5000 \
	-v $REGISTRY_HOME:/tmp/registry \
    -e STANDALONE=false \
    -e MIRROR_SOURCE=https://registry-1.docker.io \
    -e MIRROR_SOURCE_INDEX=https://index.docker.io --name registry registry

docker --registry-mirror=http://$REGISTRY_IP:5000 -d
#docker run -p 5000 -v /tmp/registry:/tmp/registry registry