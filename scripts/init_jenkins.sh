#!/bin/bash

JENKINS_PORT=8080
JENKINS_HOME=/home/jenkins
JENKINS_RUN_COMMAND="docker run -d -p $JENKINS_PORT:8080 -v $JENKINS_HOME:/var/jenkins_home --name jenkins jenkins"
JENKINS_START_COMMAND="docker start jenkins"

JENKINS_RUNNING=$(docker inspect --format="{{ .State.Running }}" jenkins 2> /dev/null)

if [ $? -eq 1 ]; then
  echo "JENKINS does not exist ..."
  echo "JENKINS run ..."
  $JENKINS_RUN_COMMAND
fi

if [ "$JENKINS_RUNNING" == "false" ]; then
  echo "JENKINS is not running."
  echo "JENKINS start"
  $JENKINS_START_COMMAND
fi

if [ "$JENKINS_RUNNING" == "true" ]; then
  echo "JENKINS is running."
fi