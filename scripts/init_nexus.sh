#!/bin/bash

NEXUS_PORT=8081
NEXUS_HOME=/home/nexus
NEXUS_RUN_COMMAND="docker run -d -p $NEXUS_PORT:8080 -v $NEXUS_HOME:/sonatype-work --name nexus mimiz/nexus"
NEXUS_START_COMMAND="docker start nexus"

NEXUS_RUNNING=$(docker inspect --format="{{ .State.Running }}" nexus 2> /dev/null)

if [ $? -eq 1 ]; then
  echo "NEXUS does not exists"
  echo "NEXUS run"
  $NEXUS_RUN_COMMAND
fi

if [ "$NEXUS_RUNNING" == "false" ]; then
  echo "NEXUS is not running."
  echo "NEXUS start"
  $NEXUS_START_COMMAND
fi

if [ "$NEXUS_RUNNING" == "true" ]; then
  echo "NEXUS is running";
fi
